//
// This is only a SKELETON file for the 'Difference Of Squares' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class Squares {
    constructor(topNumber) {
        this.numArray = new Array(topNumber);
        for (let i = 0; i < topNumber; i++) {
            this.numArray[i] = i + 1;
        }
    }

    get sumOfSquares() {
        return (this.numArray).reduce((prev, curr) => prev + curr ** 2);
    }

    get squareOfSum() {
        return ((this.numArray).reduce((prev, curr) => prev + curr)) ** 2;
    }

    get difference() {
        return this.squareOfSum - this.sumOfSquares;
    }
}
