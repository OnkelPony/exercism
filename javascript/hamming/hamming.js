//
// This is only a SKELETON file for the 'Hamming' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const compute = (left, right) => {
    if (left !== "" && right === "") {
        throw new Error('right strand must not be empty');
    } else if (left === "" && right !== "") {
        throw new Error('left strand must not be empty');
    } else if (left.length !== right.length) {
        throw new Error('left and right strands must be of equal length');
    }

    let hDistance = 0;
    [...left].forEach((base, index) => {
        if (base !== right[index]) {
            hDistance++;
        }
    });
    return hDistance;
};
