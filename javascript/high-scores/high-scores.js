//
// This is only a SKELETON file for the 'High Scores' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class HighScores {
    constructor(scores) {
        this.scoreArray = scores;
        this.sortedScores = [...this.scores].sort((a, b) => b - a);
    }

    get scores() {
        return this.scoreArray;
    }

    get latest() {
        return this.scores[this.scores.length - 1];
    }

    get personalBest() {
        console.log(this.sortedScores);
        return this.sortedScores[0];
    }

    get personalTopThree() {
        return this.sortedScores.slice(0, 3);
    }
}
