//
// This is only a SKELETON file for the 'Armstrong Numbers' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const isArmstrongNumber = (numberIn) => {
    if (numberIn === 0) {
        return true;
    }

    let numberArr = [];
    let number = numberIn;

    while (number > 0) {
        numberArr.unshift(number % 10);
        number = number / 10 | 0;
    }

    return numberIn === numberArr.reduce((total, digit) => total + Math.pow(digit, numberArr.length), 0);
};
