//
// This is only a SKELETON file for the 'Anagram' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const findAnagrams = (word, candidates) => {
    return candidates.filter(arraysEqual);

    function arraysEqual(element) {
        let a = element.toLowerCase();
        let b = word.toLowerCase();
        if (a === b) {
            return false;
        }
        return [...a].sort().join() === [...b].sort().join();
    }
};
