//
// This is only a SKELETON file for the 'D&D Character' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
import {randomInt} from "crypto";

const toSubtract = 10;
const minThrow = 3;
const maxThrow = 18;

export const abilityModifier = (score) => {
    if (score < minThrow) {
        throw new Error('Ability scores must be at least 3');
    } else if (score > maxThrow) {
        throw new Error('Ability scores can be at most 18');
    }
    return Math.floor((score - toSubtract) / 2);
};

export class Character {
    str = Character.rollAbility();
    dex = Character.rollAbility();
    con = Character.rollAbility();
    int = Character.rollAbility();
    wis = Character.rollAbility();
    cha = Character.rollAbility();

    get strength() {
        return this.str;
    }

    get dexterity() {
        return this.dex;
    }

    get constitution() {
        return this.con;
    }

    get intelligence() {
        return this.int;
    }

    get wisdom() {
        return this.wis;
    }

    get charisma() {
        return this.cha;
    }

    get hitpoints() {
        return toSubtract + abilityModifier(this.constitution);
    }

    static rollAbility() {
        return randomInt(minThrow, maxThrow);
    }
}
