//This is only a SKELETON file for the 'Series' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class Series {
    constructor(series) {
        if (!series) throw new Error('series cannot be empty');
        this.series = series;
    }

    slices(sliceLength) {
        if (sliceLength === 0) throw new Error('slice length cannot be zero');
        else if (sliceLength < 0) throw new Error('slice length cannot be negative');
        else if (sliceLength > this.series.length) {
            throw new Error('slice length cannot be greater than series length');
        }
        const digits = [...this.series].map(Number);
        return digits.slice(sliceLength - 1).map((elem, index) =>
            digits.slice(index, index + sliceLength));
    }
}