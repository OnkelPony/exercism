//
// This is only a SKELETON file for the 'Perfect Numbers' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const classify = (natNum) => {
    if (natNum < 1) {
        throw new Error('Classification is only possible for natural numbers.');
    } else if (natNum === 1 || natNum === 2) {
        return "deficient";
    }

    let factors = 0;
    for (let i = 2; i <= Math.sqrt(natNum); i++) {
        if (natNum % i === 0) {
            factors += i;
            if (natNum / i !== i) {
                factors += natNum / i;
            }
        }
    }
    factors++;

    if (factors > natNum) {
        return "abundant";
    } else if (factors < natNum) {
        return "deficient";
    } else {
        return "perfect";
    }
};


