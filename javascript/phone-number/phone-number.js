//
// This is only a SKELETON file for the 'Phone Number' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const clean = (badPhone) => {
    let goodPhone = badPhone.replace(/[()+ \-.]/g, "");
    console.log("number:" + goodPhone + " number length:" + goodPhone.length);
    if (goodPhone.length < 10) {
        throw new Error('Incorrect number of digits');
    } else if (goodPhone.length > 11) {
        throw new Error('More than 11 digits');
    } else if (goodPhone.length === 11) {
        if (goodPhone.slice(0, 1) === "1") {
            goodPhone = goodPhone.slice(1);
        } else {
            throw new Error('11 digits must start with 1');
        }
    }
    if (/[a-z]/.test(goodPhone)) {
        throw new Error('Letters not permitted');
    } else if (/[!?:]/.test(goodPhone)) {
        throw new Error('Punctuations not permitted');
    } else if (goodPhone.slice(0, 1) === "0") {
        throw new Error('Area code cannot start with zero')

    } else if (goodPhone.slice(0, 1) === "1") {
        throw new Error('Area code cannot start with one')

    } else if (goodPhone.slice(3, 4) === "0") {
        throw new Error('Exchange code cannot start with zero')

    } else if (goodPhone.slice(3, 4) === "1") {
        throw new Error('Exchange code cannot start with one')

    }
    return goodPhone;
};
