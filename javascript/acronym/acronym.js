//
// This is only a SKELETON file for the 'Acronym' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const parse = (sentence) => {
    let sentArray = sentence.split(/[ -]/);
    return sentArray.map(word => word.replace(/^_|_$/, "")[0]).join("").toUpperCase();
};
