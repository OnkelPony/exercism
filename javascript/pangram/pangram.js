//
// This is only a SKELETON file for the 'Pangram' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
const alphabet = 'abcdefghijklmnopqrstuvwxyz';

export const isPangram = (sentence) => [...alphabet].every(letter => sentence.toLowerCase().includes(letter));