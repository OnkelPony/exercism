//
// This is only a SKELETON file for the 'Darts' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const score = (x, y) => {
    let dist = Math.sqrt(x * x + y * y);
    if (dist <= 1) {
        return 10;
    } else if (dist <= 5) {
        return 5;
    } else if (dist <= 10) {
        return 1;
    }
    return 0;
};
