//
// This is only a SKELETON file for the 'ETL' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const transform = (oldScores) => {
    let result = {};
    for (let key in oldScores) {
        oldScores[key].forEach(element => result[element.toLowerCase()] = +key);
    }
    return result;
};
