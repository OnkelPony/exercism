//
// This is only a SKELETON file for the 'Isogram' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const isIsogram = (sentence) => {
    let letters = {};
    return [...sentence.toLowerCase()]
        .filter(letter => (/[^ -]/.test(letter)))
        .every(letter => {
            let isUnique = !(letters[letter]);
            letters[letter] = 1;
            return isUnique;
        });
};