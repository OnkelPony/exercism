//
// This is only a SKELETON file for the 'Scale Generator' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
const sharps = [
    'C',
    'C#',
    'D',
    'D#',
    'E',
    'F',
    'F#',
    'G',
    'G#',
    'A',
    'A#',
    'B',
];

const flats = [
    'C',
    'Db',
    'D',
    'Eb',
    'E',
    'F',
    'Gb',
    'G',
    'Ab',
    'A',
    'Bb',
    'B',
];

const useFlats = ["F", "Bb", "Eb", "Ab", "Db", "Gb", "d", "g", "c", "f", "bb", "eb"];

export class Scale {
    constructor(tonic) {
        this.tonic = tonic;
    }

    chromatic() {
        return this.interval("mmmmmmmmmmmm");
    }

    interval(intervals) {
        let result = [];
        let useChroma = sharps;

        if (useFlats.includes(this.tonic)) {
            useChroma = flats;
        }

        let index = useChroma.indexOf(this.tonic.replace(/^./, str => str.toUpperCase()));
        result.push(useChroma[index]);

        [...intervals].forEach(interval => {
            switch (interval) {
                case "m":
                    index++;
                    break;
                case "M":
                    index += 2;
                    break;
                default:
                    index += 3;
            }
            result.push(useChroma[index % 12]);
        });
        result.pop();
        return result;
    }
}
