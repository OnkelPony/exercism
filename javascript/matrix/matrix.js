//
// This is only a SKELETON file for the 'Matrix' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class Matrix {
    constructor(nuString) {
        this.resultMatrix = [];
        this.nuMatrix = nuString.split("\n");
        this.nuMatrix.forEach(row => this.resultMatrix.push(row.split(" ").map(element => Number(element))));
    }

    get rows() {
        return this.resultMatrix;
    }

    get columns() {
        return this.resultMatrix[0].map((_, i) => this.resultMatrix.map(row => row[i]));
    }
}
