//
// This is only a SKELETON file for the 'Protein Translation' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
const mapping = {
    AUG: "Methionine",
    UUU: "Phenylalanine",
    UUC: "Phenylalanine",
    UUA: "Leucine",
    UUG: "Leucine",
    UCU: "Serine",
    UCC: "Serine",
    UCA: "Serine",
    UCG: "Serine",
    UAU: "Tyrosine",
    UAC: "Tyrosine",
    UGU: "Cysteine",
    UGC: "Cysteine",
    UGG: "Tryptophan"
};

const codonLen = 3;

const stopCodons = ["UAA", "UAG", "UGA"];

export const translate = (codons = "") => {
    let result = [];
    let validCodons = Object.keys(mapping);
    let cycles = Math.ceil(codons.length / codonLen);
    for (let i = 0; i < cycles; i++) {
        let codon = codons.slice(i * codonLen, i * codonLen + codonLen);
        if (!validCodons.includes(codon)) {
            if (stopCodons.includes(codon)) {
                break;
            } else {
                throw new Error('Invalid codon');
            }
        }
        result.push(mapping[codon]);
    }
    return result;
};
