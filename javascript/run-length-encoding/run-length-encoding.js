//
// This is only a SKELETON file for the 'Run Length Encoding' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

function buildEnc(repCount, letter) {
    if (repCount === 1) {
        return letter;
    } else {
        return repCount.toString() + letter;
    }
}

export const encode = (text) => {
    let repCount = 1;
    let prevLetter = text[0];
    let result = "";
    for (let i = 1; i < text.length; i++) {
        if (text[i] === prevLetter) {
            repCount++;
        } else {
            result += buildEnc(repCount, prevLetter);
            repCount = 1;
        }
        if (i === text.length - 1) {
            result += buildEnc(repCount, text[i]);
        }
        prevLetter = text[i];
    }
    return result;
};

function buildDec(repCount, letter) {
    if (repCount === 1) {
        return letter;
    }
    return letter.repeat(repCount);
}

export const decode = (encText) => {
    let repCount = 1;
    let repCountString = "";
    let result = "";
    for (const item of encText) {
        if (!isNaN(item) && item !== " ") {
            repCountString += item;
            repCount = 0;
        } else {
            result += buildDec(+repCountString + repCount, item);
            repCountString = "";
            repCount = 1;
        }
    }
    return result;
};
