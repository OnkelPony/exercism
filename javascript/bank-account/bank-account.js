//
// This is only a SKELETON file for the 'Bank Account' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class BankAccount {
    saldo;

    constructor() {
        this.saldo = 0;
        this.isOpen = false;
    }

    get balance() {
        if (this.isOpen) {
            return this.saldo;
        } else {
            throw new ValueError();
        }
    }

    open() {
        if (!this.isOpen) {
            this.isOpen = true;
        } else {
            throw new ValueError();
        }
    }

    close() {
        if (this.isOpen) {
            this.isOpen = false;
            this.saldo = 0;
        } else {
            throw new ValueError();
        }
    }

    deposit(amount) {
        if (this.isOpen && amount >= 0) {
            this.saldo += amount;
        } else {
            throw new ValueError();
        }
    }

    withdraw(amount) {
        if (this.isOpen && amount <= this.saldo && amount >= 0) {
            this.saldo -= amount;
        } else {
            throw new ValueError();
        }
    }
}


export class ValueError extends Error {
    constructor() {
        super('Bank account error');
    }
}
