//
// This is only a SKELETON file for the 'Matching Brackets' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const isPaired = (expression) => {
    const brackets = "[]{}()";
    for (const char of expression) {
        if (!brackets.includes(char)) {
            // console.log("piča!");
            continue;
        }
        console.log(char);
    }
};
